;; Bootstrap quelpa
(if (require 'quelpa nil t)
    (quelpa-self-upgrade)
  (with-temp-buffer
    (url-insert-file-contents
     "https://framagit.org/steckerhalter/quelpa/raw/master/bootstrap.el")
    (eval-buffer)))

(setq quelpa-stable-p t)

;; Install quelpa-use-package, which will install use-package as well
(quelpa
 '(quelpa-use-package
   :fetcher git
   :url "https://framagit.org/steckerhalter/quelpa-use-package.git"
   :stable nil))
(require 'quelpa-use-package)
(package-initialize)

(use-package htmlize :quelpa)
(use-package lua-mode :quelpa)
(use-package fennel-mode :quelpa (:stable nil) :after lua-mode)
(use-package org
  :quelpa
  :after (fennel-mode htmlize)
  :config
  (defun ftb/tangle ()
    (org-babel-tangle-file "fennel-the-book.org"))
  (defun ftb/export-html ()
    (setq org-confirm-babel-evaluate nil
          org-html-htmlize-output-type 'css
          org-export-allow-bind-keywords t)
    ;; one of these should work, depending on the org version
    (with-demoted-errors (org-babel-do-load-languages 'org-babel-load-languages '((sh . t))))
    (with-demoted-errors (org-babel-do-load-languages 'org-babel-load-languages '((shell . t))))
    (with-current-buffer (find-file "fennel-the-book.org")
      (font-lock-flush)
      (font-lock-fontify-buffer)
      (org-html-export-to-html nil))))
