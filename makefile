.PHONY: tangle
tangle: fennel-the-book.org
	emacs -Q --batch -L "$(shell pwd)" -l 'fennel-the-book' --eval '(ftb/tangle)'

.PHONY: site
site: site/index.html site/fennel-the-book.css site/theme.css

site/index.html: fennel-the-book.html
	mkdir -p site && cp fennel-the-book.html site/index.html

site/fennel-the-book.css: fennel-the-book.css
	mkdir -p site && cp fennel-the-book.css site/fennel-the-book.css

site/theme.css: theme.css
	mkdir -p site && cp theme.css site/theme.css

fennel-the-book.html: fennel-the-book.org
	emacs -Q --batch -L "$(shell pwd)" -l 'fennel-the-book' --eval '(ftb/export-html)'

fennel-the-book.css: fennel-the-book.org
	make tangle

theme.css: fennel-the-book.org
	make tangle

fennel-the-book.fnl: fennel-the-book.org
	make tangle

test.fnl: fennel-the-book.org
	make tangle

.PHONY: test
test: fennel-the-book.fnl test.fnl
	fennel test.fnl -v
